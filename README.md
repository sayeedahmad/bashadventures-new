### Overview

BASHAdventures  is a terminal-based top-down adventures game written solely in
BASH. Because no external programs are used, it acts as an example of how to
perform all kinds of tasks within the BASH scripting language.

### Hosted version.

There is currently a hosted version of this game available by ssh: `ssh ahte@play.bashadventures.io`.

### Deobfuscation

BASHAdventures uses many features of the BASH scripting language which are designed
to be compact and are therefore not easily understood. This section of the README 
will act as a guide to those features.

1. `printf "\033c"` tells the terminal to print a special character that the terminal then interprets as an the "Reset Device" command which clears the screen completely. For more: http://www.termsys.demon.co.uk/vtansi.htm
2. `printf "\033[%s;%sH" "${ROW}" "${COLUMN}"` tells the terminal to move the cursor to the specified row and column within the terminal. This is used to replace individual characters so that reprinting the whole screen is not required. For more: http://www.termsys.demon.co.uk/vtansi.htm
3. `! read -r -t .5` This is used as a builtin replacement for sleep. The `-t` argument is the time (in seconds) that the command should wait. The exclamation point negates a failed state (no input is recieved) and ensures the script does not end.
4. `! read -r _` Pauses the script until the enter key is pressed. Discards all input before the enter key. The exclamation point negates a failed state (no input is recieved) and ensures the script doesn't end.
