FROM bash

WORKDIR /opt/bashadventures
COPY bashadventures .
COPY images images
COPY story story
COPY places places
COPY credits .
COPY templates/save.template .
CMD ["bash","/opt/bashadventures/bashadventures"]
