#! /usr/bin/env bash

set -ep

mkdir -p /tmp/bashadventures-debug
rm -rf /tmp/bashadventures-debug/saves/*
rm -f /tmp/bashaventures-debug/log.txt
bash -x bashadventures -l 30 -c 65 -s /tmp/bashadventures-debug/saves 2> /tmp/bashadventures-debug/log.txt
