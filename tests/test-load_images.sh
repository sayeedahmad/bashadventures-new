#! /usr/bin/env bash
set -o pipefail -o errexit

printf "Testing the load_images function:\n"

declare -i COUNTER=0
declare -i FAIL_COUNTER=0
source functions/load_images
load_images

for IMAGE in images/*; do
  if [[ ! "${IMAGES[$IMAGE]}" == "$(< "${IMAGE}")" ]]; then
    printf "\t\033[0;31mFAIL: Unable to load %s!\033[0m\n" "${IMAGE}"
    FAIL_COUNTER=$((++FAIL_COUNTER))
  fi
  COUNTER=$((++COUNTER))
done


if [[ ${FAIL_COUNTER} -gt 0 ]]; then
  printf "\t\033[0;31mFAIL: %d/%d images loaded.\033[0m\n" $((COUNTER - FAIL_COUNTER)) $((COUNTER)) 
  exit 1
else
  printf "\t\033[0;32mPASS: %d/%d images loaded.\033[0m\n" $((COUNTER - FAIL_COUNTER)) $((COUNTER)) 
fi
