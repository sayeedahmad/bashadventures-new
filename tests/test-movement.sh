#! /usr/bin/env bash
set -o pipefail -o errexit -x

printf "Testing the movement functions:\n"

declare -i COUNTER=0
declare -i FAIL_COUNTER=0
declare FAIL='n'
declare -A KEYS=
KEYS['a']='4 5'
KEYS['s']='5 6'
KEYS['d']='6 5'
KEYS['w']='5 4'
KEYS['D']='4 5'
KEYS['B']='5 6'
KEYS['C']='6 5'
KEYS['A']='5 4'
KEYS['l']='5 5'
source functions/movement


for KEY in ${!KEYS[*]}; do
  # shellcheck disable=SC2076
  if [[ ! "$(printf "%s" "${KEY}" | movement 5 5)" =~ "${KEYS[${KEY}]}" ]]; then
    printf "\t\033[0;31mFAIL: %s did not behave as expected!\033[0m\n" "${KEY}"
    FAIL_COUNTER=$((++FAIL_COUNTER))
  fi
  COUNTER=$((++COUNTER))
done

if [[ ${FAIL_COUNTER} -gt 0 ]]; then
  printf "\t\033[0;31mFAIL: %d/%d keys behaved as expected.\033[0m\n" $((COUNTER - FAIL_COUNTER)) $((COUNTER)) 
  FAIL='y'
else
  printf "\t\033[0;32mPASS: %d/%d keys behaved as expected.\033[0m\n" $((COUNTER - FAIL_COUNTER)) $((COUNTER)) 
fi


if [[ "$(move_blank 5 5 6 5)" =~ '6 5' ]]; then
  printf '\t\033[0;32mPASS: move_blank function returned as expected with good input.\033[0m\n'
else
  printf '\t\033[0;31mFAIL: move_blank function failed to return as expected with good input.\033[0m\n'
  FAIL='y'
fi

if [[ "$(move_down_wall 5 5 5 6)" =~ '5 5' ]]; then
  printf '\t\033[0;32mPASS: move_down_wall function returned as expected with good input.\033[0m\n'
else
  printf '\t\033[0;31mFAIL: move_down_wall function failed to return as expected with good input.\033[0m\n'
  FAIL='y'
fi

if [[ "$(move_down_wall 5 5 5 4)" =~ '5 6' ]]; then
  printf '\t\033[0;31mFAIL: move_down_wall function failed to fail as expected with bad input.\033[0m\n'
  FAIL='y'
else
  printf '\t\033[0;32mPASS: move_down_wall function failed as expected with bad input.\033[0m\n'
fi

if [[ "$(move_up_wall 5 5 5 4)" =~ '5 5' ]]; then
  printf '\t\033[0;32mPASS: move_up_wall function returned as expected with good input.\033[0m\n'
else
  printf '\t\033[0;31mFAIL: move_up_wall function failed to return as expected with good input.\033[0m\n'
  FAIL='y'
fi

if [[ "$(move_up_wall 5 5 5 6)" =~ '5 4' ]]; then
  printf '\t\033[0;31mFAIL: move_up_wall function failed to fail as expected with bad input.\033[0m\n'
  FAIL='y'
else
  printf '\t\033[0;32mPASS: move_up_wall function failed as expected with bad input.\033[0m\n'
fi

if [[ "${FAIL}" == 'y' ]]; then
  exit 1
fi
