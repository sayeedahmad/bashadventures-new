#! /usr/bin/env bash
set -o pipefail -o errexit

source functions/screen_fit

printf "Testing the screenfit:\n"

printf "\tTesting small screen:\n"
declare -i LINES=11
declare -i COLUMNS=22

declare SF_RESULTS

SF_RESULTS="$(echo | check_screen_fit)"

if [[ ${SF_RESULTS} =~ "11 lines" ]]; then
  printf "\t\t\033[0;32mPASS: Correct number of lines detected and returned.\033[0m\n"
else
  printf "\t\t\033[0;31mFAIL: Incorrect number of lines detected and returned.\033[0m\n"
  printf "%s" "${SF_RESULTS}"
  exit 1
fi

if [[ ${SF_RESULTS} =~ "22 columns" ]]; then
  printf "\t\t\033[0;32mPASS: Correct number of columns detected and returned.\033[0m\n"
else
  printf "\t\t\033[0;31mFAIL: Incorrect number of columns detected and returned.\033[0m\n"
  printf "%s" "${SF_RESULTS}"
  exit 1
fi


printf "\tTesting big screen:\n"
declare -i LINES=100
declare -i COLUMNS=200

declare SF_RESULTS

SF_RESULTS="$(echo | check_screen_fit)"

if [[ ${SF_RESULTS} =~ "100 lines" ]]; then
  printf "\t\t\033[0;32mPASS: Correct number of lines detected and returned.\033[0m\n"
else
  printf "\t\t\033[0;31mFAIL: Incorrect number of lines detected and returned.\033[0m\n"
  printf "%s" "${SF_RESULTS}"
  exit 1
fi

if [[ ${SF_RESULTS} =~ "200 columns" ]]; then
  printf "\t\t\033[0;32mPASS: Correct number of columns detected and returned.\033[0m\n"
else
  printf "\t\t\033[0;31mFAIL: Incorrect number of columns detected and returned.\033[0m\n"
  printf "%s" "${SF_RESULTS}"
  exit 1
fi


printf "\tTesting proper screen size:\n"
declare -i LINES=40
declare -i COLUMNS=70

declare SF_RESULTS

SF_RESULTS="$(echo | check_screen_fit)"

if [[ "${SF_RESULTS}" ]]; then
  printf "\t\t\033[0;31mFAIL: Results returned, this should not happened.\033[0m\n"
  exit 1
else
  printf "\t\t\033[0;32mPASS: No results returned, this is expected.\033[0m\n"
fi
