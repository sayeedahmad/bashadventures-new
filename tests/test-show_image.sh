#! /usr/bin/env bash
set -o pipefail -o errexit

printf "Testing the show_image function:\n"

declare -i COUNTER=0
declare -i FAIL_COUNTER=0
declare -r TEST_TEXT='The quick brown fox jumps over the lazy dog'
source functions/load_images
source functions/show_image
load_images

for IMAGE in images/*; do
  printf "Testing %s                                                \r" "${IMAGE}"
  EXPECTED_CONTENT="${IMAGES[$IMAGE]}

${TEST_TEXT}"
  ACTUAL_CONTENT="$(show_image -t "${IMAGE}" "${TEST_TEXT}")"
  if [[ ! "${ACTUAL_CONTENT}" == "${EXPECTED_CONTENT}" ]]; then
    printf "\t\033[0;31mFAIL: Unable to show %s!\033[0m\n" "${IMAGE}"
    FAIL_COUNTER=$((++FAIL_COUNTER))
  fi
  COUNTER=$((++COUNTER))
done


if [[ ${FAIL_COUNTER} -gt 0 ]]; then
  printf "\t\033[0;31mFAIL: %d/%d images shown.\033[0m\n" $((COUNTER - FAIL_COUNTER)) $((COUNTER))
  exit 1
else
  printf "\t\033[0;32mPASS: %d/%d images shown.\033[0m\n" $((COUNTER - FAIL_COUNTER)) $((COUNTER))
fi
